#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <time.h>

#define CLIENT_PORT 8001
#define SERVER_PORT 8000
#define LIVE_TIME 10
#define BUFFER_SIZE 50

int main(int argc, char *argv[])
{

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0); // this socket_fd have to be added to interested list ---- Add socnonblock
    //Client socket init
    const struct sockaddr_in socket_addr = 
    {
        .sin_family = AF_INET,
        .sin_port = htons(CLIENT_PORT),
        .sin_addr.s_addr = htonl(INADDR_ANY),
    };
    bind(socket_fd, (struct sockaddr *)&socket_addr, sizeof(socket_addr));
    //Server socket struct
    const struct sockaddr_in server_addr = 
    {
        .sin_family = AF_INET,
        .sin_port = htons(SERVER_PORT),
        .sin_addr.s_addr = htonl(INADDR_ANY),
    };
    //Start conversation
    connect(socket_fd,(struct sockaddr *)&server_addr,sizeof(socket_addr));
    char buffer[BUFFER_SIZE];
    if (argc > 1)
    {
        sprintf(buffer,"%s:message",argv[1]);
        send(socket_fd,buffer,strlen(buffer),0);
    }
    char recv_buffer[BUFFER_SIZE];
    recv(socket_fd,recv_buffer,BUFFER_SIZE,0);
    printf("%s1\n1",recv_buffer);
    //Timer
    time_t _time = time(NULL);
    while((time(NULL) - _time) < LIVE_TIME);
    close(socket_fd);
    return 0;
}