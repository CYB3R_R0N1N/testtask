#include <stdio.h>
#include <sys/epoll.h>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>

#include <string.h>

#define MAX_EVENTS 10
#define SERVER_PORT 8000
#define MAX_CONN_QUEUE 10
#define MAX_CLIENTS MAX_CONN_QUEUE

//Callback prototype
typedef void (*Callback)(void *arg, int epoll_fd, int fd, uint32_t events);
//Function prototypes
void recieve_callback(void *arg, int epoll_fd, int fd, uint32_t events);
void send_callback(void *arg, int epoll_fd, int fd, uint32_t events);
void add_event(int epoll_fd, int socket_fd, struct epoll_event *event, Callback callback);
void edit_event(int epoll_fd, int socket_fd, struct epoll_event *event, Callback callback);
void remove_event(int epoll_fd, int socket_fd, struct epoll_event *event);
void add_client();
void remove_client();

void add_client(int fd)
{
    for (int i = 0 ; i < MAX_CLIENTS; i++)
    {
        if (clients_table[i] == 0 || clients_table[i] == fd)
        {
            clients_table[i] = fd;
            return;
        }
    }
}

void remove_client(int fd)
{
    for (int i = 0 ; i < MAX_CLIENTS; i++)
    {
        if (clients_table[i] == fd)
        {
            clients_table[i] = 0;
            return;
        }
    }
}

void accept_callback(void *arg, int epoll_fd, int fd, uint32_t events)
{
    int connection = accept(fd,NULL,NULL);
    if (connection == -1)
    {
        printf("Connection failed\n");
        return;
    }
    int flags = fcntl(fd,F_GETFL,0);
    fcntl(fd,F_SETFL,flags | O_NONBLOCK);
    struct epoll_event ev =
    {
        .events = EPOLLIN,
        .data = (epoll_data_t)connection
    };
    add_client(connection);
    add_event(epoll_fd, connection, &ev, recieve_callback);
    printf("Connected\n");
}

void recieve_callback(void *arg, int epoll_fd, int fd, uint32_t events)
{
    char buffer[500]; // add while
    ssize_t read;
    read = recv(fd,buffer,200,0); //Could be while

    if (read == 0) // Client close connection
    {
        printf("Client closed connection\n\n");
        remove_client(fd);
        remove_event(epoll_fd,fd,NULL);
        return;
    }

    printf("Recieved: %s\n",buffer);

    struct epoll_event ev =
    {
        .events = EPOLLOUT,
        .data = (epoll_data_t)fd
    };
    edit_event(epoll_fd, fd, &ev, send_callback);
}

void send_callback(void *arg, int epoll_fd, int fd, uint32_t events)
{
    //const char buffer[100] = "Hello world\r\n";
    char response[50] = "ACK\n";
    // sprintf(response,"HTTP/1.1 200 OK\r\nContent-Length: %zd\r\nContent-Type: "
    //         "text/html\r\n\r\n%s",strlen(buffer),buffer);
    send(fd, response, strlen(response), 0); // Exception here because client close connection
    struct epoll_event ev =
    {
        .events = EPOLLIN,
        .data = (epoll_data_t)fd
    };
    edit_event(epoll_fd, fd, &ev, recieve_callback);
}

typedef struct callbacks_cells
{
    int fd;
    Callback callback;
} CallbackCell;

CallbackCell table[MAX_EVENTS];
int clients_table[MAX_CLIENTS];

void add_event(int epoll_fd, int socket_fd, struct epoll_event *event, Callback callback)
{
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket_fd, event);
    for (int i = 0 ; i< MAX_EVENTS; i++)
    {
        if (table[i].fd == 0 || table[i].fd == socket_fd)
        {
            table[i].fd = socket_fd;
            table[i].callback = callback;
            return;
        }
    }
}

void edit_event(int epoll_fd, int socket_fd, struct epoll_event *event, Callback callback)
{
    epoll_ctl(epoll_fd, EPOLL_CTL_MOD, socket_fd, event);
    for (int i = 0 ; i< MAX_EVENTS; i++)
    {
        if (table[i].fd == socket_fd)
        {
            table[i].callback = callback;
        }
    }
}

void remove_event(int epoll_fd, int socket_fd, struct epoll_event *event)
{
    epoll_ctl(epoll_fd, EPOLL_CTL_DEL, socket_fd, event);
    for (int i = 0 ; i< MAX_EVENTS; i++)
    {
        if (table[i].fd == 0)
        {
            table[i].fd = 0;
            table[i].callback = NULL;
        }
    }
}

Callback find_callback(int fd)
{
    for (int i = 0 ; i < MAX_EVENTS; i++)
    {
        if (table[i].fd == fd)
            return table[i].callback;
    }
}

int main()
{
    for (int i = 0; i < MAX_EVENTS; i++)
    {
        table[i].fd = 0;
        table[i].callback = NULL;
    }
    for (int i = 0; i < MAX_CLIENTS; i++)
        clients_table[i] = 0;

    /* 
        Create and start socket
    */
    int socket_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0); // this socket_fd have to be added to interested list ---- Add socnonblock
    const struct sockaddr_in socket_addr = 
    {
        .sin_family = AF_INET,
        .sin_port = htons(SERVER_PORT),
        .sin_addr.s_addr = htonl(INADDR_ANY),
    };
    bind(socket_fd, (struct sockaddr *)&socket_addr, sizeof(socket_addr));
    listen(socket_fd,MAX_CONN_QUEUE);

    //Create reactor and start it
    struct epoll_event events[MAX_EVENTS];

    int epoll_fd;
    epoll_fd = epoll_create(10);

    struct epoll_event ev =
    {
        .events = EPOLLIN,
        .data = (epoll_data_t)socket_fd
    };
    add_event(epoll_fd,socket_fd,&ev,accept_callback);


    printf("Waiting for connection\n");

    while(1)
    {
        int result = epoll_wait(epoll_fd,events,MAX_EVENTS,5000); // -1 for evelasting block , 0 for instant return

        switch (result)
        {
        case -1:
            printf("Error in epoll_wait()\n");
            break;
        case 0:
            break;
        default:
            for (int i = 0; i < result; i++)
            {
                int fd = events[i].data.fd;
                Callback callback = find_callback(fd);
                callback(NULL,epoll_fd,fd,events->events);
            }
            break;
        }
    }

    close(socket_fd);
    close(epoll_fd);
    return 0;
}