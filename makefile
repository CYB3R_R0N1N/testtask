BUILD_ARGS = -g
BUILD_DIR = build


.PHONY: all clean server client 

all: server client

direcrory: 
	mkdir -p $(BUILD_DIR)

server.o: direcrory
	gcc -c server.c -o $(BUILD_DIR)/server.o $(BUILD_ARGS)
server: server.o
	gcc -o $(BUILD_DIR)/server $(BUILD_DIR)/server.o $(BUILD_ARGS)

client.o: direcrory
	gcc -c client.c -o $(BUILD_DIR)/client.o $(BUILD_ARGS)
client: client.o
	gcc -o $(BUILD_DIR)/client $(BUILD_DIR)/client.o $(BUILD_ARGS)

clean:
	rm -rf $(BUILD_DIR)/client $(BUILD_DIR)/server $(BUILD_DIR)/*.o 
